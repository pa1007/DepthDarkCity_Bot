package fr.cloud4you.pa1007.depthdarkcity_bot.listener.admin;

import fr.cloud4you.pa1007.depthdarkcity_bot.DepthDarkCity_Bot;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.DiscordListener;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.member.UserPardonEvent;

public class UserPardonListener extends DiscordListener implements IListener<UserPardonEvent> {

    public UserPardonListener(DepthDarkCity_Bot discordBots) {
        super(discordBots);
    }

    @Override
    public String getName() {
        return "Pardon Listener";
    }

    @Override
    public void handle(UserPardonEvent event) {

    }
}
