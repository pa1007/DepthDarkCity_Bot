package fr.cloud4you.pa1007.depthdarkcity_bot.listener.admin;

import fr.cloud4you.pa1007.depthdarkcity_bot.DepthDarkCity_Bot;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.DiscordListener;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.voice.VoicePingEvent;

public class PingVoiceListener extends DiscordListener implements IListener<VoicePingEvent> {

    public PingVoiceListener(DepthDarkCity_Bot discordBots) {
        super(discordBots);
    }

    @Override
    public String getName() {
        return "Ping Listener";
    }

    @Override
    public void handle(VoicePingEvent event) {

    }
}
