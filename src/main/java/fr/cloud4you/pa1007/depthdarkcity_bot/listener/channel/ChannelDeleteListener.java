package fr.cloud4you.pa1007.depthdarkcity_bot.listener.channel;

import fr.cloud4you.pa1007.depthdarkcity_bot.DepthDarkCity_Bot;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.DiscordListener;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.channel.ChannelDeleteEvent;

public class ChannelDeleteListener extends DiscordListener implements IListener<ChannelDeleteEvent> {

    public ChannelDeleteListener(DepthDarkCity_Bot discordBots) {
        super(discordBots);
    }

    @Override
    public String getName() {
        return "Delete Listener";
    }

    @Override
    public void handle(ChannelDeleteEvent event) {

    }
}
