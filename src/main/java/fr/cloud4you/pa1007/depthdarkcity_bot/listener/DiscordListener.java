package fr.cloud4you.pa1007.depthdarkcity_bot.listener;

import fr.cloud4you.pa1007.depthdarkcity_bot.DepthDarkCity_Bot;
import java.util.Objects;

public abstract class DiscordListener implements DiscordListenerMain {

    protected final DepthDarkCity_Bot pluginInstance;

    public DiscordListener(DepthDarkCity_Bot discordBots) {
        Objects.requireNonNull(discordBots);
        this.pluginInstance = discordBots;
    }
}
