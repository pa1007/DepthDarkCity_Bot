package fr.cloud4you.pa1007.depthdarkcity_bot.listener.mainlistener;

import fr.cloud4you.pa1007.depthdarkcity_bot.DepthDarkCity_Bot;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.DiscordListener;
import sx.blah.discord.api.events.Event;
import sx.blah.discord.api.events.IListener;

public class BotListener extends DiscordListener implements IListener<Event> {

    public BotListener(DepthDarkCity_Bot discordBots) {
        super(discordBots);
    }

    @Override
    public String getName() {
        return "MainListener";
    }

    @Override
    public void handle(Event event) {
        System.out.println(event);
    }
}
