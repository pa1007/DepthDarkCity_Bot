package fr.cloud4you.pa1007.depthdarkcity_bot.listener.guild;

import fr.cloud4you.pa1007.depthdarkcity_bot.DepthDarkCity_Bot;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.DiscordListener;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.member.UserJoinEvent;

public class UserJoinGuildListener extends DiscordListener implements IListener<UserJoinEvent> {

    public UserJoinGuildListener(DepthDarkCity_Bot discordBots) {
        super(discordBots);
    }

    @Override
    public String getName() {
        return "Join Guild Listener";
    }

    @Override
    public void handle(UserJoinEvent event) {

    }
}
