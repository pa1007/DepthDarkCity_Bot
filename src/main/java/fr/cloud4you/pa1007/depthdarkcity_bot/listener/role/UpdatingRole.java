package fr.cloud4you.pa1007.depthdarkcity_bot.listener.role;

import fr.cloud4you.pa1007.depthdarkcity_bot.DepthDarkCity_Bot;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.DiscordListener;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.member.UserRoleUpdateEvent;

public class UpdatingRole extends DiscordListener implements IListener<UserRoleUpdateEvent> {

    public UpdatingRole(DepthDarkCity_Bot discordBots) {
        super(discordBots);
    }

    @Override
    public String getName() {
        return "User Role Update Listener";
    }

    @Override
    public void handle(UserRoleUpdateEvent event) {

    }
}
