package fr.cloud4you.pa1007.depthdarkcity_bot.listener.mod;

import fr.cloud4you.pa1007.depthdarkcity_bot.DepthDarkCity_Bot;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.DiscordListener;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageUpdateEvent;

public class MessageUpdateListener extends DiscordListener implements IListener<MessageUpdateEvent> {

    public MessageUpdateListener(DepthDarkCity_Bot discordBots) {
        super(discordBots);
    }

    @Override
    public String getName() {
        return "Message Update Listener";
    }

    @Override
    public void handle(MessageUpdateEvent event) {

    }
}
