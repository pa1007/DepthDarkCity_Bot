package fr.cloud4you.pa1007.depthdarkcity_bot.listener.voice;

import fr.cloud4you.pa1007.depthdarkcity_bot.DepthDarkCity_Bot;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.DiscordListener;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.voice.user.UserVoiceChannelMoveEvent;

public class UserMoveChannelListener extends DiscordListener implements IListener<UserVoiceChannelMoveEvent> {

    public UserMoveChannelListener(DepthDarkCity_Bot discordBots) {
        super(discordBots);
    }

    @Override
    public String getName() {
        return "User Move Listener";
    }

    @Override
    public void handle(UserVoiceChannelMoveEvent event) {

    }
}
