package fr.cloud4you.pa1007.depthdarkcity_bot.listener.voice;

import fr.cloud4you.pa1007.depthdarkcity_bot.DepthDarkCity_Bot;
import fr.cloud4you.pa1007.depthdarkcity_bot.creator.WaitingUser;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.DiscordListener;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.voice.user.UserVoiceChannelLeaveEvent;
import sx.blah.discord.handle.obj.IRole;
import java.util.List;

public class LeaveVoiceChannelListener extends DiscordListener implements IListener<UserVoiceChannelLeaveEvent> {

    public LeaveVoiceChannelListener(DepthDarkCity_Bot discordBots) {
        super(discordBots);
    }

    @Override
    public String getName() {
        return "User Leave Channel Listener";
    }

    @Override
    public void handle(UserVoiceChannelLeaveEvent event) {
        List<IRole> roleList = event.getUser().getRolesForGuild(event.getGuild());
        switch (event.getVoiceChannel().getStringID()) {
            case "457222745562808321":
                if (roleList.contains(pluginInstance.getClient().getRoleByID(457523984481648654L))) {
                    break;
                }
                if (pluginInstance.getWaitingUserID().containsKey(event.getUser().getLongID())) {
                    WaitingUser waitingUser = pluginInstance.getWaitingUserID().remove(event.getUser().getLongID());
                    event.getVoiceChannel().leave();
                    pluginInstance.getClient().getMessageByID(waitingUser.getAdminMessageID()).delete();
                    pluginInstance.setChannel1Open(true);
                }
                break;
            case "457222878274912257":
                if (roleList.contains(pluginInstance.getClient().getRoleByID(457523984481648654L))) {
                    break;
                }
                if (pluginInstance.getWaitingUserID().containsKey(event.getUser().getLongID())) {
                    WaitingUser waitingUser = pluginInstance.getWaitingUserID().remove(event.getUser().getLongID());
                    event.getVoiceChannel().leave();
                    pluginInstance.getClient().getMessageByID(waitingUser.getAdminMessageID()).delete();
                    pluginInstance.setChannel2Open(true);
                }
                break;
        }
    }
}
