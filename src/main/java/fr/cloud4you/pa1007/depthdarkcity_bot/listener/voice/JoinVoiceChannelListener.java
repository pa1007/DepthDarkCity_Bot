package fr.cloud4you.pa1007.depthdarkcity_bot.listener.voice;

import fr.cloud4you.pa1007.depthdarkcity_bot.DepthDarkCity_Bot;
import fr.cloud4you.pa1007.depthdarkcity_bot.creator.WaitingUser;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.DiscordListener;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.voice.user.UserVoiceChannelJoinEvent;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.util.audio.AudioPlayer;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.time.Instant;
import java.util.Date;
import java.util.List;

public class JoinVoiceChannelListener extends DiscordListener implements IListener<UserVoiceChannelJoinEvent> {

    private IMessage    message;
    private WaitingUser waitingUser;

    public JoinVoiceChannelListener(DepthDarkCity_Bot discordBots) {
        super(discordBots);
    }

    @Override
    public String getName() {
        return "User Join Channel Listener";
    }

    @Override
    public void handle(UserVoiceChannelJoinEvent event) {
        List<IRole> roleList = event.getUser().getRolesForGuild(event.getGuild());
        switch (event.getVoiceChannel().getStringID()) {

            case "457222745562808321":
                if (!pluginInstance.isForceClose1()) {
                    if (pluginInstance.isChannel1Open()) {
                        if (roleList.contains(pluginInstance.getClient().getRoleByID(457523984481648654L))) {
                            return;
                        }
                        if (pluginInstance.getWaitingUserID().containsKey(event.getUser().getLongID())) {
                            return;
                        }
                    }
                    else {
                        return;
                    }
                }
                else {
                    return;
                }

                break;
            case "457222878274912257":
                if (!pluginInstance.isForceClose2()) {
                    if (pluginInstance.isChannel2Open()) {
                        if (roleList.contains(pluginInstance.getClient().getRoleByID(457523984481648654L))) {
                            return;
                        }
                        if (pluginInstance.getWaitingUserID().containsKey(event.getUser().getLongID())) {
                            return;
                        }
                    }
                    else {
                        return;
                    }
                }
                else {
                    return;
                }
                break;
        }

        event.getVoiceChannel().join();
        AudioPlayer audioP = AudioPlayer.getAudioPlayerForGuild(event.getGuild());
        audioP.clear();
        audioP.setVolume(1);
        try {
            audioP.queue(getClass().getResource("/admin/waitingaudio.mp3"));
        }
        catch (IOException | UnsupportedAudioFileException e) {
            e.printStackTrace();
        }
        message = event.getClient().getChannelByID(457520483080863756L).sendMessage("User : "
                                                                                    + event.getUser().mention()
                                                                                    + " as besoin d'aide ! venez l'aider");
        waitingUser = new WaitingUser(
                event.getVoiceChannel().getLongID(),
                message.getLongID(),
                event.getUser().getLongID()
        );
        waitingUser.setMomentOfJoin(Date.from(Instant.now()));
        pluginInstance.getWaitingUserID().put(event.getUser().getLongID(), waitingUser);
        pluginInstance.getWaitingMessageID().put(message.getLongID(), waitingUser);
        pluginInstance.setChannel2Open(false);
    }
}
