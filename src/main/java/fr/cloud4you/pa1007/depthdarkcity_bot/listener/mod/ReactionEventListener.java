package fr.cloud4you.pa1007.depthdarkcity_bot.listener.mod;

import fr.cloud4you.pa1007.depthdarkcity_bot.DepthDarkCity_Bot;
import fr.cloud4you.pa1007.depthdarkcity_bot.creator.WaitingUser;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.DiscordListener;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.channel.message.reaction.ReactionAddEvent;
import sx.blah.discord.handle.impl.obj.ReactionEmoji;
import sx.blah.discord.util.audio.AudioPlayer;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.time.Instant;
import java.util.Date;

public class ReactionEventListener extends DiscordListener implements IListener<ReactionAddEvent> {

    public ReactionEventListener(DepthDarkCity_Bot discordBots) {
        super(discordBots);
    }

    @Override
    public String getName() {
        return "Message Reaction Listener";
    }

    @Override
    public void handle(ReactionAddEvent event) {

        if (event.getUser() != pluginInstance.getClient().getOurUser()) {
            if (pluginInstance.getWaitingMessageID().containsKey(event.getMessageID())) {
                if (event.getReaction().getEmoji().equals(ReactionEmoji.of("checked", 457672565633318915L))) {
                    WaitingUser waitingUser = pluginInstance.getWaitingMessageID().get(event.getMessageID());
                    if (!waitingUser.isInProgress()) {
                        waitingUser.setInProgress(true);
                        waitingUser.setAdminUserId(event.getUser().getLongID());
                        waitingUser.setRespondDate(Date.from(Instant.now()));
                        waitingUser.setAdminName(event.getUser().getName());
                        pluginInstance.getWaitingMessageID().replace(event.getMessageID(), waitingUser);
                        pluginInstance.getWaitingUserID().replace(waitingUser.getUserLongID(), waitingUser);
                        AudioPlayer audioP = AudioPlayer.getAudioPlayerForGuild(event.getGuild());
                        audioP.clear();
                        audioP.setVolume(1);
                        try {
                            audioP.queue(getClass().getResource("/admin/admincomming.mp3"));
                        }
                        catch (IOException | UnsupportedAudioFileException e) {
                            e.printStackTrace();
                            event.getUser().getOrCreatePMChannel().sendMessage(
                                    "Une erreur est arrivée lors de la creation du message , merci de faire remonter cette erreur !");
                        }

                        event.getUser().getOrCreatePMChannel().sendMessage("Tu as pris la demande de : "
                                                                           + pluginInstance.getClient().getUserByID(
                                waitingUser.getUserLongID()).mention()
                                                                           + " ( "
                                                                           + waitingUser.getUserLongID()
                                                                           + " ) , Merci de rejoindre le canal "
                                                                           + pluginInstance.getClient().getVoiceChannelByID(
                                waitingUser.getChannelLongId()).getName()
                                                                           + "Merci une fois fini de taper les commandes : "
                                                                           + "```!needhelp "
                                                                           + waitingUser.getUserLongID()
                                                                           + " {Resumer}```");
                        event.getMessage().edit(event.getMessage().getContent()
                                                + " pris par l'admin : "
                                                + event.getUser().mention());
                    }
                }
            }
        }
    }
}
