package fr.cloud4you.pa1007.depthdarkcity_bot.listener.mod;

import fr.cloud4you.pa1007.depthdarkcity_bot.DepthDarkCity_Bot;
import fr.cloud4you.pa1007.depthdarkcity_bot.creator.ProblemType;
import fr.cloud4you.pa1007.depthdarkcity_bot.creator.WaitingUser;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.DiscordListener;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.IVoiceChannel;

public class MessageReceiveListener extends DiscordListener implements IListener<MessageReceivedEvent> {

    public MessageReceiveListener(DepthDarkCity_Bot discordBots) {
        super(discordBots);
    }

    @Override
    public String getName() {
        return "Message Receive Listener";
    }

    @Override
    public void handle(MessageReceivedEvent event) {
        if (!event.getAuthor().equals(pluginInstance.getClient().getOurUser())) {
            String[] args = event.getMessage().getContent().split(" ");

            if (args[0].toLowerCase().equals("!needhelp")) {
                if (args.length > 2) {
                    switch (args[1]) {

                        case "help":
                            helpNeedHelp(event, args);
                            break;
                        case "stop":
                            stopNeedHelp(event, args);
                            break;
                        case "change":
                            changeNeedHelp(event, args);
                            break;
                        case "pause":
                            pauseNeedHelp(event, args);
                            break;
                        case "move":
                            moveNeedHelp(event, args);
                            break;
                        case "resume":
                            resumeNeedHelp(event, args);
                            break;
                        case "type":
                            typeNeedHelp(event, args);
                            break;
                        case "create":
                            createNeedHelp(event, args);
                            break;
                        default:
                            helpNeedHelp(event, args);
                            break;
                    }
                }
            }


        }
    }

    private void createNeedHelp(MessageReceivedEvent event, String[] args) {

    }

    private void typeNeedHelp(MessageReceivedEvent event, String[] args) {
        if (args.length < 3) {
            sendPrivateMessage(
                    event.getAuthor(),
                    "Need more arguments Usage : !needHelp type <UserID> [CHEAT|REFUND|HELP|MISTAKE|TROLLING|NOTING|OTHER]"
            );
            event.getMessage().delete();
        }
        else if (args.length > 4) {
            sendPrivateMessage(
                    event.getAuthor(),
                    "too much arguments Usage : !needHelp type <UserID> [CHEAT|REFUND|HELP|MISTAKE|TROLLING|NOTING|OTHER]"
            );
            event.getMessage().delete();
        }
        else {
            String id = args[2];
            IUser  user;
            try {
                user = event.getClient().getUserByID(Long.parseLong(id));
            }
            catch (Exception e) {
                sendPrivateMessage(event.getAuthor(), "The player is unknown or wrong id");
                event.getMessage().delete();
                return;
            }
            if (pluginInstance.getWaitingUserID().containsKey(user.getLongID())) {
                String      type        = args[3].toUpperCase();
                WaitingUser waitingUser = pluginInstance.getWaitingUserID().get(user.getLongID());
                switch (type) {
                    case "CHEAT":
                        changeType(user, ProblemType.CHEAT, waitingUser, event.getChannel());
                        break;
                    case "REFUND":
                        changeType(user, ProblemType.REFUND, waitingUser, event.getChannel());
                        break;
                    case "HELP":
                        changeType(user, ProblemType.HELP, waitingUser, event.getChannel());
                        break;
                    case "MISTAKE":
                        changeType(user, ProblemType.MISTAKE, waitingUser, event.getChannel());
                        break;
                    case "OTHER":
                        changeType(user, ProblemType.OTHER, waitingUser, event.getChannel());
                        break;
                    case "TROLLING":
                        changeType(user, ProblemType.TROLLING, waitingUser, event.getChannel());
                        break;
                    case "NOTING":
                        changeType(user, ProblemType.NOTING, waitingUser, event.getChannel());
                        break;
                    default:
                        sendPrivateMessage(
                                event.getAuthor(),
                                "Wrong arguments Usage : !needHelp type [CHEAT|REFUND|HELP|MISTAKE|TROLLING|NOTING|OTHER]"
                        );
                        event.getMessage().delete();
                        break;
                }
            }
            else {
                sendPrivateMessage(event.getAuthor(), "The User is not asking for help");
                event.getMessage().delete();
            }
        }

    }

    private void resumeNeedHelp(MessageReceivedEvent event, String[] args) {

    }

    private void moveNeedHelp(MessageReceivedEvent event, String[] args) {
        if (args.length < 3) {
            sendPrivateMessage(event.getAuthor(), "need more arguments Usage : !needHelp move <UserID>");
            event.getMessage().delete();
        }
        else if (args.length > 3) {
            sendPrivateMessage(event.getAuthor(), "too much arguments Usage : !needHelp move <UserID>");
            event.getMessage().delete();
        }
        else {
            IVoiceChannel channel = event.getAuthor().getVoiceStateForGuild(event.getGuild()).getChannel();
            String        id      = args[2];
            IUser         user;
            try {
                user = event.getClient().getUserByID(Long.parseLong(id));
            }
            catch (Exception e) {
                sendPrivateMessage(event.getAuthor(), "The player is unknown or wrong id");
                event.getMessage().delete();
                return;
            }
            if (pluginInstance.getWaitingUserID().containsKey(user.getLongID())) {
                if (channel != null) {
                    user.moveToVoiceChannel(channel);
                }
                else {
                    sendPrivateMessage(event.getAuthor(), "You must be in a voice channel to do that");
                    event.getMessage().delete();
                }
            }
            else {
                sendPrivateMessage(event.getAuthor(), "The User is not asking for help");
                event.getMessage().delete();
            }
        }

    }

    private void pauseNeedHelp(MessageReceivedEvent event, String[] args) {
        if (args.length < 3) {
            sendPrivateMessage(event.getAuthor(), "need more arguments Usage : !needHelp pause <ChannelID>");
            event.getMessage().delete();
        }
        else if (args.length > 3) {
            sendPrivateMessage(event.getAuthor(), "too much arguments Usage : !needHelp move <ChannelID>");
            event.getMessage().delete();
        }
        else {
            boolean temp;
            switch (args[2]) {
                case "457222745562808321":
                    temp = !pluginInstance.isForceClose1();
                    pluginInstance.setForceClose1(temp);
                    break;
                case "457222878274912257":
                    temp = !pluginInstance.isForceClose2();
                    pluginInstance.setForceClose2(temp);

                    break;
                default:
                    sendPrivateMessage(
                            event.getAuthor(),
                            "Ce canal n'est pas enregister, si c'est une erreur merci de la signaler l'id du canal à votre supérieur : "
                            + args[2]
                    );
                    event.getMessage().delete();
                    return;

            }
            event.getChannel().sendMessage((temp)
                                                   ? "Le canal est ferme merci de penser a le re-ouvrir "
                                                   : "Le canal est ouvert");
        }
    }

    private void changeNeedHelp(MessageReceivedEvent event, String[] args) {

    }

    private void stopNeedHelp(MessageReceivedEvent event, String[] args) {

    }

    private void helpNeedHelp(MessageReceivedEvent event, String[] args) {
        sendPrivateMessage(event.getAuthor(), "!needHelp [help|stop|pause|change|move|resume|type|create]");
        event.getMessage().delete();
    }

    private IMessage sendPrivateMessage(IUser user, String message) {
        return user.getOrCreatePMChannel().sendMessage(message);
    }

    private void changeType(IUser user, ProblemType type, WaitingUser waitingUser, IChannel channel) {
        waitingUser.setType(type);
        pluginInstance.getWaitingUserID().replace(user.getLongID(), waitingUser);
        pluginInstance.getWaitingMessageID().replace(waitingUser.getAdminMessageID(), waitingUser);
        channel.sendMessage("Type changed to : " + type.name());
    }
}
