package fr.cloud4you.pa1007.depthdarkcity_bot.listener.mod;

import fr.cloud4you.pa1007.depthdarkcity_bot.DepthDarkCity_Bot;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.DiscordListener;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageEditEvent;

public class MessageDeleteListener extends DiscordListener implements IListener<MessageEditEvent> {


    public MessageDeleteListener(DepthDarkCity_Bot discordBots) {
        super(discordBots);
    }

    @Override
    public String getName() {
        return "Message Edit Event";
    }

    @Override
    public void handle(MessageEditEvent event) {

    }

}
