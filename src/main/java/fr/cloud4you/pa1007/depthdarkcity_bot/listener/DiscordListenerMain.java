package fr.cloud4you.pa1007.depthdarkcity_bot.listener;

public interface DiscordListenerMain {

    /**
     * get the command name
     *
     * @return String (name of the command)
     */
    String getName();
}
