package fr.cloud4you.pa1007.depthdarkcity_bot;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.cloud4you.pa1007.depthdarkcity_bot.creator.WaitingUser;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.DiscordListenerMain;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.admin.PingVoiceListener;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.admin.UserPardonListener;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.channel.ChannelDeleteListener;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.guild.UserJoinGuildListener;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.mainlistener.BotListener;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.mod.MessageDeleteListener;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.mod.MessageReceiveListener;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.mod.MessageUpdateListener;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.mod.ReactionEventListener;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.role.UpdatingRole;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.voice.JoinVoiceChannelListener;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.voice.LeaveVoiceChannelListener;
import fr.cloud4you.pa1007.depthdarkcity_bot.listener.voice.UserMoveChannelListener;
import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.obj.ActivityType;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.StatusType;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class DepthDarkCity_Bot {

    private static final Gson              GSON =
            new GsonBuilder().serializeNulls().setPrettyPrinting().create();
    private static       DepthDarkCity_Bot instance;
    private              IDiscordClient    client;
    private              IChannel          errorChannel, reportChannel, needHelpChannel;
    private final Map<Long, WaitingUser> waitingUserID, waitingChannelID;
    private DiscordListenerMain[] discordListener;
    private IUser                 mainUser;
    private boolean               running, channel1Open, channel2Open, forceClose1, forceClose2;

    public DepthDarkCity_Bot() {
        discordListener = new DiscordListenerMain[]{
                new BotListener(this),
                new PingVoiceListener(this),
                new UserPardonListener(this),
                new ChannelDeleteListener(this),
                new UserJoinGuildListener(this),
                new MessageDeleteListener(this),
                new MessageReceiveListener(this),
                new MessageUpdateListener(this),
                new UpdatingRole(this),
                new JoinVoiceChannelListener(this),
                new LeaveVoiceChannelListener(this),
                new UserMoveChannelListener(this),
                new ReactionEventListener(this)
        };
        instance = this;
        waitingUserID = new HashMap<>();
        waitingChannelID = new HashMap<>();
        channel1Open = true;
        channel2Open = true;
        forceClose1 = false;
        forceClose2 = false;
    }

    public boolean isForceClose1() {
        return forceClose1;
    }

    public void setForceClose1(boolean forceClose1) {
        this.forceClose1 = forceClose1;
    }

    public boolean isForceClose2() {
        return forceClose2;
    }

    public void setForceClose2(boolean forceClose2) {
        this.forceClose2 = forceClose2;
    }

    public boolean isChannel1Open() {
        return channel1Open;
    }

    public void setChannel1Open(boolean channel1Open) {
        this.channel1Open = channel1Open;
    }

    public boolean isChannel2Open() {
        return channel2Open;
    }

    public void setChannel2Open(boolean channel2Open) {
        this.channel2Open = channel2Open;
    }

    public Map<Long, WaitingUser> getWaitingMessageID() {
        return waitingChannelID;
    }

    public IUser getMainUser() {
        return mainUser;
    }

    public IChannel getReportChannel() {
        return reportChannel;
    }

    public IChannel getErrorChannel() {
        return errorChannel;
    }

    public IDiscordClient getClient() {
        return client;
    }

    public File getPath() {
        return new File(ClassLoader.getSystemClassLoader().getResource(".").getPath() + "importantChannel.json");
    }

    public void stopBot() {
        client.logout();
    }

    public IChannel getNeedHelpChannel() {
        return needHelpChannel;
    }

    public Map<Long, WaitingUser> getWaitingUserID() {
        return waitingUserID;
    }

    private void initBot() {
        ClientBuilder cb = new ClientBuilder().withToken(BotConfiguration.TOKEN)
                .setPresence(
                        StatusType.ONLINE,
                        ActivityType.LISTENING,
                        BotConfiguration.MAIN_MESSAGE + " Server Modertator ! "
                );
        for (DiscordListenerMain discordListenerMain : discordListener) {
            System.out.println("Registering event {}" + String.join("/", discordListenerMain.getName()));

            cb.registerListener(discordListenerMain);
        }
        client = cb.login();
        System.out.println("Bot is started!");
        try {
            System.out.println("Registering Channel");
            errorChannel = client.getChannelByID(445647803721580574L);
            reportChannel = client.getChannelByID(450315316120125451L);
            needHelpChannel = client.getChannelByID(457520483080863756L);
            System.out.println("Channel registered");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        try {
            mainUser = client.getUserByID(178881958720307200L);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        try {

        }
        catch (Exception e) {
            e.printStackTrace();
        }
        running = true;
    }

    public static DepthDarkCity_Bot getInstance() {
        return instance;
    }

    public static void main(String[] arguments) {
        DepthDarkCity_Bot depthDarkCity_bot = new DepthDarkCity_Bot();
        depthDarkCity_bot.initBot();
        Scanner scanner = new Scanner(System.in);
        while (depthDarkCity_bot.running) {
            if (scanner.next().equals("!stop")) {
                depthDarkCity_bot.stopBot();
                break;
            }
        }
    }
}
