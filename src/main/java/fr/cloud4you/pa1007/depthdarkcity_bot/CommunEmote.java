package fr.cloud4you.pa1007.depthdarkcity_bot;

public final class CommunEmote {

    /**
     * The ddc emote :ddc:
     */
    public final static String DDC_EMOTE = "449945264866983949";

    /**
     * The red cross emote :X_:
     */
    public final static String RED_CROSS_EMOTE = "451077653869625375";

    /**
     * The minecraft debug cube emote :minecraft:
     */
    public final static String MINECRAFT_DIRT_CUBE = "449945305803128833";

    /**
     * The Debug stick Emote :stick:
     */
    public final static String DEBUG_STICK_EMOTE = "456439620557602816";

    /**
     * The server ID
     */
    public final static String SERVER_ID = "441239193134563348";
}
