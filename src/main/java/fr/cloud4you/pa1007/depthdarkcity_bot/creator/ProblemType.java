package fr.cloud4you.pa1007.depthdarkcity_bot.creator;

import com.google.common.base.MoreObjects;

public enum ProblemType {
    CHEAT("Triche"),
    REFUND("Demande de Rembourcement"),
    HELP("Demande d'aide"),
    TROLLING("Pour du troll in-GAME"),
    NOTING("Rien"),
    MISTAKE("Juste une erreur de leur part"),
    OTHER("autre (voir la resolution du probleme)");

    private final String text;

    ProblemType(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("text", text)
                .toString();
    }

}
