package fr.cloud4you.pa1007.depthdarkcity_bot.creator;

import java.util.Objects;
import java.util.UUID;

public class Admin {


    /**
     * the uuid of the player.
     *
     * @since 1.0
     */
    private UUID minecraftUUID;

    /**
     * the user id of admin.
     *
     * @since 1.0
     */
    private String userID;

    /**
     * the user that give him the perm.
     *
     * @since 1.0
     */
    private String givenByUserID;

    /**
     * the userName of the person.
     *
     * @since 1.0
     */
    private String userName;

    /**
     * the level of permission.
     *
     * @since 1.0
     */
    private int permissionLevel;

    public Admin(String userID, String givenByUserID, String userName, int permissionLevel) {
        this.userID = userID;
        this.givenByUserID = givenByUserID;
        this.userName = userName;
        this.permissionLevel = permissionLevel;
    }

    /**
     * @return the uuid of the player.
     * @since 1.0
     */
    public UUID getMinecraftUUID() {
        return this.minecraftUUID;
    }

    /**
     * Sets the <code>minecraftUUID</code> field.
     *
     * @param minecraftUUID the uuid of the player.
     * @since 1.0
     */
    public void setMinecraftUUID(UUID minecraftUUID) {
        this.minecraftUUID = minecraftUUID;
    }

    /**
     * @return the userName of the person.
     * @since 1.0
     */
    public String getUserName() {
        return this.userName;
    }

    /**
     * Sets the <code>userName</code> field.
     *
     * @param userName the userName of the person.
     * @since 1.0
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the user that give him the perm.
     * @since 1.0
     */
    public String getGivenByUserID() {
        return this.givenByUserID;
    }

    /**
     * Sets the <code>givenByUserID</code> field.
     *
     * @param givenByUserID the user that give him the perm.
     * @since 1.0
     */
    public void setGivenByUserID(String givenByUserID) {
        this.givenByUserID = givenByUserID;
    }

    /**
     * @return the level of permission.
     * @since 1.0
     */
    public int getPermissionLevel() {
        return this.permissionLevel;
    }

    /**
     * Sets the <code>permissionLevel</code> field.
     *
     * @param permissionLevel the level of permission.
     * @since 1.0
     */
    public void setPermissionLevel(int permissionLevel) {
        this.permissionLevel = permissionLevel;
    }

    /**
     * @return the user id of admin.
     * @since 1.0
     */
    public String getUserID() {
        return this.userID;
    }

    /**
     * Sets the <code>userID</code> field.
     *
     * @param userID the user id of admin.
     * @since 1.0
     */
    public void setUserID(String userID) {
        this.userID = userID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Admin)) {
            return false;
        }
        Admin admin = (Admin) o;
        return permissionLevel == admin.permissionLevel &&
               Objects.equals(userID, admin.userID) &&
               Objects.equals(givenByUserID, admin.givenByUserID) &&
               Objects.equals(userName, admin.userName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(userID, givenByUserID, userName, permissionLevel);
    }

    @Override
    public String toString() {
        return com.google.common.base.MoreObjects.toStringHelper(this)
                .add("userID", userID)
                .add("givenByUserID", givenByUserID)
                .add("userName", userName)
                .add("permissionLevel", permissionLevel)
                .toString();
    }
}
