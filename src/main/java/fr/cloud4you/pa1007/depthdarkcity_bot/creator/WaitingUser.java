package fr.cloud4you.pa1007.depthdarkcity_bot.creator;

import com.google.common.base.MoreObjects;
import fr.cloud4you.pa1007.depthdarkcity_bot.DepthDarkCity_Bot;
import java.time.Instant;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class WaitingUser {

    private DepthDarkCity_Bot instance = DepthDarkCity_Bot.getInstance();
    //Add redirection/ save database / Stop bot for a specific channel / report command/

    /**
     * to redirect the player to another person.
     *
     * @since 1.0
     */
    private long redirectedUserID;

    /**
     * if the person is redirected to another person.
     *
     * @since 1.0
     */
    private boolean redirected;

    /**
     * to see if an admin has taken the lead.
     *
     * @since 1.0
     */
    private boolean inProgress;

    /**
     * The type of problem that the person had.
     *
     * @since 1.0
     */
    private ProblemType type;

    /**
     * The long id of the channel where the user wait.
     *
     * @since 1.0
     */
    private long channelLongId;

    /**
     * The admin name.
     *
     * @since 1.0
     */
    private String adminName;

    /**
     * The id of the "Response".
     *
     * @since 1.0
     */
    private UUID id;

    /**
     * For what the person was here.
     *
     * @since 1.0
     */
    private String demand;

    /**
     * tell if the user has been help.
     *
     * @since 1.0
     */
    private boolean answer;

    /**
     * The date that the admin has respond.
     *
     * @since 1.0
     */
    private Date respondDate;

    /**
     * the admin id .
     *
     * @since 1.0
     */
    private long adminUserId;

    /**
     * The date the user ask for help.
     *
     * @since 1.0
     */
    private Date momentOfJoin;

    /**
     * the id of the message.
     *
     * @since 1.0
     */
    private long adminMessageID;

    /**
     * The id of the user waiting.
     *
     * @since 1.0
     */
    private long userLongID;

    public WaitingUser(long channelLongId, long adminMessageID, long userLongID) {
        this.channelLongId = channelLongId;
        this.adminMessageID = adminMessageID;
        this.userLongID = userLongID;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    /**
     * @return to redirect the player to another person.
     * @since 1.0
     */
    public long getRedirectedUserID() {
        return this.redirectedUserID;
    }

    /**
     * Sets the <code>redirectedUserID</code> field.
     *
     * @param redirectedUserID to redirect the player to another person.
     * @since 1.0
     */
    public void setRedirectedUserID(long redirectedUserID) {
        this.redirectedUserID = redirectedUserID;
    }

    /**
     * @return if the person is redirected to another person.
     * @since 1.0
     */
    public boolean isRedirected() {
        return this.redirected;
    }

    /**
     * Sets the <code>redirected</code> field.
     *
     * @param redirected if the person is redirected to another person.
     * @since 1.0
     */
    public void setRedirected(boolean redirected) {
        this.redirected = redirected;
    }

    /**
     * @return to see if an admin has taken the lead.
     * @since 1.0
     */
    public boolean isInProgress() {
        return this.inProgress;
    }

    /**
     * Sets the <code>inProgress</code> field.
     *
     * @param inProgress to see if an admin has taken the lead.
     * @since 1.0
     */
    public void setInProgress(boolean inProgress) {
        this.inProgress = inProgress;
    }

    /**
     * @return The type of problem that the person had.
     * @since 1.0
     */
    public ProblemType getType() {
        return this.type;
    }

    /**
     * Sets the <code>type</code> field.
     *
     * @param type The type of problem that the person had.
     * @since 1.0
     */
    public void setType(ProblemType type) {
        this.type = type;
    }

    /**
     * @return The admin name.
     * @since 1.0
     */
    public String getAdminName() {
        return this.adminName;
    }

    /**
     * Sets the <code>adminName</code> field.
     *
     * @param adminName The admin name.
     * @since 1.0
     */
    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    /**
     * @return The id of the "Response".
     * @since 1.0
     */
    public UUID getId() {
        return this.id;
    }

    /**
     * Sets the <code>id</code> field.
     *
     * @param id The id of the "Response".
     * @since 1.0
     */
    public void setId(UUID id) {
        this.id = id;
    }

    /**
     * @return For what the person was here.
     * @since 1.0
     */
    public String getDemand() {
        return this.demand;
    }

    /**
     * Sets the <code>demand</code> field.
     *
     * @param demand For what the person was here.
     * @since 1.0
     */
    public void setDemand(String demand) {
        this.demand = demand;
    }

    /**
     * @return tell if the user has been help.
     * @since 1.0
     */
    public boolean getAnswer() {
        return this.answer;
    }

    /**
     * Sets the <code>answer</code> field.
     *
     * @param answer tell if the user has been help.
     * @since 1.0
     */
    public void setAnswer(boolean answer) {
        this.answer = answer;
    }

    /**
     * @return The date that the admin has respond.
     * @since 1.0
     */
    public Date getRespondDate() {
        return this.respondDate;
    }

    /**
     * Sets the <code>respondDate</code> field.
     *
     * @param respondDate The date that the admin has respond.
     * @since 1.0
     */
    public void setRespondDate(Date respondDate) {
        this.respondDate = respondDate;
    }

    /**
     * @return the admin id .
     * @since 1.0
     */
    public long getAdminUserId() {
        return this.adminUserId;
    }

    /**
     * Sets the <code>adminUserId</code> field.
     *
     * @param adminUserId the admin id .
     * @since 1.0
     */
    public void setAdminUserId(long adminUserId) {
        this.adminUserId = adminUserId;
    }

    /**
     * @return The date the user ask for help.
     * @since 1.0
     */
    public Date getMomentOfJoin() {
        return this.momentOfJoin;
    }

    /**
     * Sets the <code>momentOfJoin</code> field.
     *
     * @param momentOfJoin The date the user ask for help.
     * @since 1.0
     */
    public void setMomentOfJoin(Date momentOfJoin) {
        this.momentOfJoin = momentOfJoin;
    }

    /**
     * @return the id of the message.
     * @since 1.0
     */
    public long getAdminMessageID() {
        return this.adminMessageID;
    }

    /**
     * Sets the <code>adminMessageID</code> field.
     *
     * @param adminMessageID the id of the message.
     * @since 1.0
     */
    public void setAdminMessageID(long adminMessageID) {
        this.adminMessageID = adminMessageID;
    }

    /**
     * @return The long id of the channel where the user wait.
     * @since 1.0
     */
    public long getChannelLongId() {
        return this.channelLongId;
    }

    /**
     * Sets the <code>channelLongId</code> field.
     *
     * @param channelLongId The long id of the channel where the user wait.
     * @since 1.0
     */
    public void setChannelLongId(long channelLongId) {
        this.channelLongId = channelLongId;
    }

    /**
     * @return The id of the user waiting.
     * @since 1.0
     */
    public long getUserLongID() {
        return this.userLongID;
    }

    /**
     * Sets the <code>userLongID</code> field.
     *
     * @param userLongID The id of the user waiting.
     * @since 1.0
     */
    public void setUserLongID(long userLongID) {
        this.userLongID = userLongID;
    }

    /**
     * For the admin to make the report
     *
     * @return The report in a string format
     * @since 1.0
     */
    public String createRequest() {
        String        nameToADDAdmin = (adminName != null) ? adminName : "?";
        String        resolution     = (answer) ? " , Que j'ai résolu " : " , Que je n'ai réussi a résolvé";
        StringBuilder build          = new StringBuilder();
        return build.append("Aujourd'hui à ").append(Date.from(Instant.now())).append(" ,moi ").append(nameToADDAdmin)
                .append(" a aidé l'utilisateur ").append(instance.getClient().getUserByID(userLongID).mention())
                .append(" pour un probleme du type : ").append(type.name()).append(resolution).append(
                        "voici un petit résumé : ").append(demand).append(" ID : ").append(id).toString();

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof WaitingUser)) {
            return false;
        }
        WaitingUser that = (WaitingUser) o;
        return channelLongId == that.channelLongId &&
               answer == that.answer &&
               adminUserId == that.adminUserId &&
               adminMessageID == that.adminMessageID &&
               userLongID == that.userLongID &&
               Objects.equals(id, that.id) &&
               Objects.equals(demand, that.demand) &&
               Objects.equals(respondDate, that.respondDate) &&
               Objects.equals(momentOfJoin, that.momentOfJoin);
    }

    @Override
    public int hashCode() {

        return Objects.hash(
                channelLongId,
                id,
                demand,
                answer,
                respondDate,
                adminUserId,
                momentOfJoin,
                adminMessageID,
                userLongID
        );
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("userLongID", userLongID)
                .add("channelLongId", channelLongId)
                .add("id", id)
                .add("demand", demand)
                .add("answer", answer)
                .add("respondDate", respondDate)
                .add("adminUserId", adminUserId)
                .add("momentOfJoin", momentOfJoin)
                .add("adminMessageID", adminMessageID)
                .toString();
    }

}
