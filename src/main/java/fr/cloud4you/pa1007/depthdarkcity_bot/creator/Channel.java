package fr.cloud4you.pa1007.depthdarkcity_bot.creator;

import java.util.Objects;

public class Channel {


    /**
     * The description of the channel.
     *
     * @since 1.0
     */
    private String description;

    /**
     * The name of the channel.
     *
     * @since 1.0
     */
    private String name;

    /**
     * the long Id of the channel.
     *
     * @since 1.0
     */
    private long longID;

    public Channel(String description, String name, long longID) {
        this.description = description;
        this.name = name;
        this.longID = longID;
    }

    /**
     * @return The description of the channel.
     * @since 1.0
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Sets the <code>description</code> field.
     *
     * @param description The description of the channel.
     * @since 1.0
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The name of the channel.
     * @since 1.0
     */
    public String getName() {
        return this.name;
    }

    /**
     * Sets the <code>name</code> field.
     *
     * @param name The name of the channel.
     * @since 1.0
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the long Id of the channel.
     * @since 1.0
     */
    public long getLongID() {
        return this.longID;
    }

    /**
     * Sets the <code>longID</code> field.
     *
     * @param longID the long Id of the channel.
     * @since 1.0
     */
    public void setLongID(long longID) {
        this.longID = longID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Channel)) {
            return false;
        }
        Channel channel = (Channel) o;
        return longID == channel.longID &&
               Objects.equals(description, channel.description) &&
               Objects.equals(name, channel.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(description, name, longID);
    }

    @Override
    public String toString() {
        return com.google.common.base.MoreObjects.toStringHelper(this)
                .add("description", description)
                .add("name", name)
                .add("longID", longID)
                .toString();
    }
}
